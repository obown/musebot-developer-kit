# README #

## About ##

This is Arne Eigenfeldt’s Max Musebot Conductor folder. It has a few bells and whistles, most importantly the ability to launch ensembles.

The Max patches are in the src folder. You would need to build the actual application by launching the file Max_Musebot_Conductor.maxpat, and building the application in Max. Both Max 6 and Max 7 will work.


The Max MC launches Musebot scripts from within the application, assigns network ports to each Musebot found in the Musebot directory, and controls musebot gain and shutdown commands.

If you want to run the MC from Max instead of as a standalone, the source patch is in the /src directory. This patch should find the Musebots as per the standalone.

--

Arne Eigenfeldt

arne_e@sfu.ca