# README #

## About ##

This is the Musebot Developer Kit. You probably found it because you're interested in creating a musebot. To do that, read the specification, download the MDK (if you haven't done so already), run the musebot conductor, run some of the musebot example agents, use the example agents as a starting point for your own musebot.

## Download ##

Current version (v0.1) (zip): https://bitbucket.org/obown/musebot-developer-kit/get/musebot-developer-kit-v0.1.zip

Download the entire repository via Git: "git clone https://bitbucket.org/obown/musebot-developer-kit.git"

## What's Not There Yet ##

This kit is under development, with issue tracking at the Bitbucket repository site: https://bitbucket.org/obown/musebot-developer-kit/issues?status=new&status=open

NOTE: We have removed Max and PD standalone builds from this repo as they were bloating the size of the repo enormously. Therefore presently you won't be able to get up and running immediately. We are working to create a ready-to-play download hosted elsewhere.

Some things we hope to have soon, but don't just yet: a SuperCollider example, a swish MC with bells and whistles (current version is largely just a proof of concept to get started with), well documented examples and development resources (we're getting there).

## The Musebot Specification ##

Head over to the musebot specification, in the Documentation folder, or see the online working version [here](https://docs.google.com/document/d/1UtdLYsOErzXKNFxrM7utHeFXgPNcC_w40lTtUxtCYO8/edit#heading=h.x7g6y6m9j0i8) (comments welcome).

## Running the Musebot Conductor (MC) ##

The MC is a standalone application. We have Max and Java versions under development. Look in the respective folders under Musebot Conductor Application Currently tested on OS X only. Does it work on your system?

## Running and Hacking the Musebot Example Agents ##

The musebot example agents live in the Musebots folder. 

You can run the example agents from inside the MC application. You can also run example agents by double-clicking the run.command file in each musebot's folder, but without correct connection to the MC the musebot might not behave properly (you are recommended to run them from the MC).

## Contributing ##

You could:

* Contribute a musebot agent
* Contribute to the musebot specification
* Contribute to the musebot conductor app (or make your own)
* Contribute to the example musebot agents, or provide support libraries for your favourite environment.

The musebot project is all about a community of practitioners working together. Anything is open to discussion. Contact us with your suggestions.

## Credits ##

* [Oliver Bown](mailto:oliver.bown@sydney.edu.au) (Repository owner, Processing examples)
* [Ben Carey](mailto:contact@bencarey.net) (MaxMSP examples, PD examples, MC)
* [Arne Eigenfeldt](mailto:aeigenfeldt@gmail.com)