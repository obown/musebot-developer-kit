# Musebot Directory #

* Send contributions and corrections to [oliver.bown@sydney.edu.au](oliver.bown@sydney.edu.au).
* All 3rd party contributions are covered under their own licenses.
* Download musebots to your Musebots directory.

The Musebots:

* [Max_Keys_Examplebot](https://www.dropbox.com/s/1jyi054m18k7uw4/Max_Keys_Examplebot.zip?dl=0) - Ben Carey (file size: 35.5MB)
* [Max_Lead_Examplebot](https://www.dropbox.com/s/7hpxa5c7w5b8b0k/Max_Lead_Examplebot.zip?dl=0) - Ben Carey (file size: 33.7MB)
* [arne_TextureBot](https://www.dropbox.com/sh/39lagf0yxf6ozyw/AABuizNAYDcHlXlqqDllPEvia?dl=0) - Arne Eigenfeldt (file size: 1.06 GB, including large sample bank)