#!/bin/bash

# Where am I?
DIR=`dirname "$0"`
echo "Moving to ${DIR}."
cd "$DIR"

echo "Starting Java."
java -server -Xmx2000m -cp beads-musebot.jar org.musicalmetacreation.musebot.example_agents.KeysAgent