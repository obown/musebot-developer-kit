package org.musicalmetacreation.musebot.example_agents;

import javafx.application.Application;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.data.Buffer;
import net.beadsproject.beads.ugens.WavePlayer;

import org.jaudiolibs.beads.AudioServerIO;

public class TestAudioIO extends Application {

	public static void main(String[] args) {
		AudioContext ac = new AudioContext(new AudioServerIO.Jack());
		WavePlayer wp = new WavePlayer(ac, 500, Buffer.SINE);
		ac.out.addInput(wp);
		ac.start();
		Text text = new Text("Hello");
		System.out.println("x");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
